export enum ClientType{
    PUBLISHER = "publisher",
    SUBSCRIBER = "subscriber"
}