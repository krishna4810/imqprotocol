import { ContentType } from "../Enums/contentType";

export class IMQResponse{
    header: ResponseHeader = new ResponseHeader;
    body: ResponseBody = new ResponseBody;
}

export class ResponseHeader{
    contentType:  ContentType = ContentType.JSON;
    statusCode: any;
    timeStamp: any;
}

export class ResponseBody{
    data: any;
}