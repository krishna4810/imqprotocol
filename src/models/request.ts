import { ClientType } from "../app";
import { ContentType } from "../Enums/contentType";

export class IMQRequest{
    clientType: ClientType | undefined;
    header: RequestHeader = new RequestHeader;
    query: any;
    body: RequestBody = new RequestBody;
}

export class RequestHeader{
    contentType: ContentType = ContentType.JSON;
    source: string | undefined;
    email: string = "";
    requestType: any;
    timeStamp: any;
    topic?: any;
}

export class RequestBody{
    data: any;
}
