import { EventEmitter } from "events";
import net from 'net';
import { EventTypes } from "../constants/events.ts/EventTypes";
import { ClientType } from "../Enums/clientType";
import { IMQRequest } from "../models/request";

export class IMQServer {
    public eventEmitter: EventEmitter = new EventEmitter();
    private readonly server = net.createServer();
    createServer(ipAddress: string, port: number) {
        this.server.listen(port, ipAddress, () => {
        });
        this.setupEventListenerForClient();
        return this.eventEmitter;
    }


    private setupEventListenerForClient() {
        this.server.on('connection', (client: any) => {
            this.fireClientEvents(client);
            client.on('end', () => {
            });
        });
    }
    private fireClientEvents(client: any) {
        client.on('data', (data: any) => {
            try {
                let request: IMQRequest = this.getRequest(data);
                switch (request.clientType) {
                    case ClientType.PUBLISHER:
                        this.emitPublisherEvents(request, client);
                        break;
                    case ClientType.SUBSCRIBER:
                        this.emitSubscriberEvents(request, client);
                        break;
                    default:
                        throw new Error("Please send a valid client type in request");
                }
            } catch (error) {
                client.write("Error: ", error.message);
            }

        });
    }

    private emitSubscriberEvents(request: IMQRequest, client: any) {
        this.eventEmitter.emit(EventTypes.SUBSCRIBER_EVENTS, request, client);
    }
    private emitPublisherEvents(request: IMQRequest, client: any) {
        this.eventEmitter.emit(EventTypes.PUBLISHER_EVENTS, request, client);
    }
    private getRequest(data: any): IMQRequest {
        let request: IMQRequest = JSON.parse(data);
        if (!this.isValidRequest(request)) {
            throw new Error("Invalid request");
        } 
        return request;

    }
    private isValidRequest(request: IMQRequest):boolean {
        return !!request?.header;
    }
}
