import { EventEmitter } from 'events';
import net from 'net';
import { EventTypes } from '../constants/events.ts/EventTypes';
export class IMQClient {
    public eventEmitter: EventEmitter = new EventEmitter();
    private clientSocket: any = null;
    createClient(serverIp: string, serverPort: number) {
        this.clientSocket = net.connect({ port: serverPort }, () => {
        });
        this.setupClientListeners();
        return this.clientSocket;
    }
    public close() {
        this.clientSocket.end();
    }

    private setupClientListeners() {
        this.clientSocket.on('data', (data: string) => {
            this.eventEmitter.emit(EventTypes.SERVER_RESPONSE, JSON.parse(data));
        });

        this.clientSocket.on('end', () => {
            this.eventEmitter.emit(EventTypes.CONNECTION_ENDED);
        });
    }
}