import { IMQResponse } from "../app";

export class ResponseWriter {
    static write(connection: any, response: IMQResponse) {
        connection.write(JSON.stringify(response));
    }
}