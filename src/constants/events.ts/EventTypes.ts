export const EventTypes = {
    PUBLISHER_EVENTS: 'publisher-events',
    SUBSCRIBER_EVENTS: 'subscriber-events',
    SERVER_RESPONSE: 'server-response',
    CONNECTION_ENDED:'client-connection-ended'
}