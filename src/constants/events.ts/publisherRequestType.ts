export const PublisherRequestType = {
    REGISTER: 'register-publisher',
    PUBLISH: 'publish-message',
    GET_ALL_TOPIC:'get-all-topics'
}