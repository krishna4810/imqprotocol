export const SubscriberRequestType = {
    REGISTER: 'register-subscriber',
    PULL: 'pull',
    SUBSCRIBE_TO_TOPIC: 'subscribe-to-topic',
    AKNOWLEDGE: 'acknowledge-for-recieved-messages',
    GET_ALL_TOPICS: 'get-all-topics',
    GET_SUBSCIBED_TOPICS:'get-all-topics'
}